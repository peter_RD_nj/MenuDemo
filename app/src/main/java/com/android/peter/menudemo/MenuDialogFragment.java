package com.android.peter.menudemo;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

/**
 * Created by peter on 2018/3/13.
 */

public class MenuDialogFragment extends DialogFragment {
    private final static String TAG = "MenuDialogFragment";

    private Context mContext;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = getContext();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String[] menuItems = new String[]{"Option1","Option2","Option3"};
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
        alertDialog.setItems(menuItems, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(mContext,"Option " + (i + 1),Toast.LENGTH_SHORT).show();
            }
        });

        return alertDialog.create();
    }
}
