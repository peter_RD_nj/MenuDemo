package com.android.peter.menudemo;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

public class MenuDemoActivity extends AppCompatActivity {

    private Context mContext;
    private TextView mContextMenu;
    private TextView mPopupMenu;
    private TextView mAlertDialog;
    private TextView mDialogFragment;
    private TextView mBottomSheetDialog;
    private TextView mPopupWindow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_demo);
        mContext = this;

        //ContextMenu
        mContextMenu = findViewById(R.id.tv_context_menu);
        registerForContextMenu(mContextMenu);

        //PopupMenu
        mPopupMenu = findViewById(R.id.tv_popup_menu);
        mPopupMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopupMenu();
            }
        });

        //AlertDialog
        mAlertDialog = findViewById(R.id.tv_alert_dialog);
        mAlertDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAlertDialog();
            }
        });

        //DialogFragment
        mDialogFragment = findViewById(R.id.tv_dialog_fragment);
        mDialogFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogFragment();
            }
        });

        //BottomSheetDialog
        mBottomSheetDialog  = findViewById(R.id.tv_bottom_sheet_dialog);
        mBottomSheetDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                showBottomSheetDialog();
                showBottomSheetDialogWithListView();
            }
        });

        //PopupWindow
        mPopupWindow = findViewById(R.id.tv_popup_window);
        mPopupWindow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopupWindow();
            }
        });
    }

    private void showPopupMenu(){
        PopupMenu popupMenu = new PopupMenu(this,mPopupMenu);
        popupMenu.inflate(R.menu.menu_main);
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.action_1:
                        Toast.makeText(mContext,"Option 1",Toast.LENGTH_SHORT).show();
                        return true;
                    case R.id.action_2:
                        Toast.makeText(mContext,"Option 2",Toast.LENGTH_SHORT).show();
                        return true;
                    case R.id.action_3:
                        Toast.makeText(mContext,"Option 3",Toast.LENGTH_SHORT).show();
                        return true;
                    default:
                        //do nothing
                }

                return false;
            }
        });
        popupMenu.show();
    }

    private void showAlertDialog() {
        String[] menuItems = new String[]{"Option1","Option2","Option3"};
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
        alertDialog.setItems(menuItems, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(mContext,"Option " + (i + 1),Toast.LENGTH_SHORT).show();
            }
        });
        alertDialog.show();
    }

    private void showDialogFragment() {
        MenuDialogFragment menuDialogFragment = new MenuDialogFragment();
        menuDialogFragment.show(getFragmentManager(),"menu_dialog_fragment");
    }

    private void showBottomSheetDialog() {
        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(mContext);
        View view = LayoutInflater.from(mContext).inflate(R.layout.popup_window_menu, null);
        TextView menuItem1 = view.findViewById(R.id.tv_option1);
        menuItem1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext,"Option 1",Toast.LENGTH_SHORT).show();
                if(bottomSheetDialog != null) {
                    bottomSheetDialog.dismiss();
                }
            }
        });
        TextView menuItem2 = view.findViewById(R.id.tv_option2);
        menuItem2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext,"Option 2",Toast.LENGTH_SHORT).show();
                if(bottomSheetDialog != null) {
                    bottomSheetDialog.dismiss();
                }
            }
        });
        TextView menuItem3 = view.findViewById(R.id.tv_option3);
        menuItem3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext,"Option 3",Toast.LENGTH_SHORT).show();
                if(bottomSheetDialog != null) {
                    bottomSheetDialog.dismiss();
                }
            }
        });
        bottomSheetDialog.setContentView(view);
        bottomSheetDialog.setCancelable(true);
        bottomSheetDialog.setCanceledOnTouchOutside(true);
        bottomSheetDialog.show();
    }

    private void showBottomSheetDialogWithListView() {
        final BottomSheetDialog dialog=new BottomSheetDialog(mContext);
        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.bottom_sheet_dialog_menu,null);
        ListView listView= view.findViewById(R.id.lv_bottom_sheet_dialog);
        String[] array=new String[]{"item-0","item-1","item-2","item-3","item-4","item-5","item-6","item-7","item-8",
                "item-9","item-10","item-11","item-12","item-13","item-14","item-15"};
        ArrayAdapter adapter=new ArrayAdapter(mContext,android.R.layout.simple_list_item_1,array);
        listView.setAdapter(adapter);

        dialog.setContentView(view);
        dialog.show();

    }

    private void showPopupWindow() {
        final PopupWindow popupWindow = new PopupWindow(mContext);

        View view = LayoutInflater.from(mContext).inflate(R.layout.popup_window_menu,null);
        TextView menuItem1 = view.findViewById(R.id.tv_option1);
        menuItem1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext,"Option 1",Toast.LENGTH_SHORT).show();
                if(popupWindow != null) {
                    popupWindow.dismiss();
                }
            }
        });
        TextView menuItem2 = view.findViewById(R.id.tv_option2);
        menuItem2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext,"Option 2",Toast.LENGTH_SHORT).show();
                if(popupWindow != null) {
                    popupWindow.dismiss();
                }
            }
        });
        TextView menuItem3 = view.findViewById(R.id.tv_option3);
        menuItem3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext,"Option 3",Toast.LENGTH_SHORT).show();
                if(popupWindow != null) {
                    popupWindow.dismiss();
                }
            }
        });

        popupWindow.setContentView(view);
        popupWindow.setWidth(480);
        popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popupWindow.setBackgroundDrawable(new ColorDrawable(0xffffffff));
        popupWindow.setTouchable(true);
        popupWindow.setOutsideTouchable(true);
        if(!popupWindow.isShowing()) {
            popupWindow.showAtLocation(mPopupWindow,Gravity.CENTER,0,0);
        }
    }


    //创建OptionsMenu，加载我们之前定义的menu_main.xml布局文件
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return true;
    }

    //当OptionsMenu被选中的时候处理具体的响应事件
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_1:
                Toast.makeText(mContext,"Option 1",Toast.LENGTH_SHORT).show();
                return true;
            case R.id.action_2:
                Toast.makeText(mContext,"Option 2",Toast.LENGTH_SHORT).show();
                return true;
            case R.id.action_3:
                Toast.makeText(mContext,"Option 3",Toast.LENGTH_SHORT).show();
                return true;
            default:
                //do nothing
        }
        return super.onOptionsItemSelected(item);
    }

    //创建ContextMenu，加载我们之前定义的menu_main.xml布局文件
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.menu_main, menu);
    }

    //当ContextMenu被选中的时候处理具体的响应事件
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_1:
                Toast.makeText(mContext,"Option 1",Toast.LENGTH_SHORT).show();
                return true;
            case R.id.action_2:
                Toast.makeText(mContext,"Option 2",Toast.LENGTH_SHORT).show();
                return true;
            case R.id.action_3:
                Toast.makeText(mContext,"Option 3",Toast.LENGTH_SHORT).show();
                return true;
            default:
                //do nothing
        }
        return super.onContextItemSelected(item);
    }
}
