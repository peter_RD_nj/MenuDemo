# MenuDemo
各种菜单实现方法汇总demo

## Google推荐的三种基本的菜单

- 选项菜单（OptionsMenu）
- 上下文菜单（ContextMenu）
- 弹出菜单（PopupMenu）

## 其他方法

- AlertDialog
- DialogFragment
- PopupWindow
- BottomSheetDialog
- Spinner

博客链接：  
[Android中的菜单实现汇总](https://www.jianshu.com/p/9129434db073)
[Android推荐的三种基本菜单](https://www.jianshu.com/p/df9216cf57a7)